for i in range(3):
    print(i)

for i in range(3, 10):
    print(i, end=" ")

for i in range(9, 4, -1):
    print(i, end=", ")

for c in "La libertaire":
    print(c)


def function():
    variable = "Hello from function"
    print(variable)


function()

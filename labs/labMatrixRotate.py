from random import randint
from enum import Enum


class MatrixDirection(Enum):
    LEFT = "влево"
    RIGHT = "вправо"
    UPSIDE_DOWN = "отразить"


def rotateAndPrintMatrix(m, direction):
    matrix_width = len(m[0])
    matrix_height = len(m)

    if direction == MatrixDirection.UPSIDE_DOWN.value:
        rotated = m[::-1]
        print('\n'.join([''.join(['{:3}'.format(item) for item in row])
                         for row in rotated]))
    elif direction == MatrixDirection.RIGHT.value:
        for j in range(matrix_width):
            for i in range(matrix_height - 1, -1, -1):
                print(m[i][j], end=" ")
            print()
    elif direction == MatrixDirection.LEFT.value:
        for j in range(matrix_width - 1, -1, -1):
            for i in range(matrix_height):
                print(m[i][j], end=" ")
            print()
    else:
        print("Не могу определить направление поворота")


def createRandomMatrix(width: int, height: int) -> list[list[int]]:
    if width != 0 and height != 0:
        return [[randint(10, 99) for _ in range(width)] for _ in range(height)]


if __name__ == '__main__':
    w = int(input("Введите ширину матрицы: "))
    h = int(input("Введите высоту матрицы: "))
    matrix = createRandomMatrix(w, h)
    print('\n'.join([''.join(['{:3}'.format(item) for item in row])
                     for row in matrix]))
    d = input("Введите направление поворота матрицы: ").lower()
    rotateAndPrintMatrix(matrix, d)

from enum import Enum


class FrameType(Enum):
    SINGLE = "одинарная"
    DOUBLE = "двойная"


def drawFrame(width, height, type):
    if type == FrameType.SINGLE.value:
        drawSingleFrame(width, height)
    elif type == FrameType.DOUBLE.value:
        drawDoubleFrame(width, height)


def drawSingleFrame(width, height):
    print("┌" + ("─" * (width - 2)) + "┐", end="")
    print(("\n│" + (" " * (width - 2)) + "│") * (height - 2))
    print("└" + ("─" * (width - 2)) + "┘")


def drawDoubleFrame(width, height):
    print("╔" + ("═" * (width - 2)) + "╗", end="")
    print(("\n║" + (" " * (width - 2)) + "║") * (height - 2))
    print("╚" + ("═" * (width - 2)) + "╝")


if __name__ == '__main__':
    width = int(input("Введите ширину рамки: "))
    height = int(input("Введите высоту рамки: "))
    type = input("Введите тип рамки: ").lower()
    drawFrame(width, height, type)
    input()

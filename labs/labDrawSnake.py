def drawSnake(width, height):
    from_left = False
    for i in range(0, height):
        if i % 2 == 0:
            print("*" * width)
            from_left = not from_left
        elif from_left:
            print(" " * (width - 1) + "*")
        else:
            print("*")


if __name__ == '__main__':
    drawSnake(5, 5)
    drawSnake(100, 100)

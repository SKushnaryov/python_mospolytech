for i in range(0, 16):
    for j in range(0, 16):
        number = i * 16 + j
        print(f"\u001b[38;5;{number}m {number} \u001b[0m", end=" ")
    print()
    for k in range(0, 16):
        number = i * 16 + k
        print(f"\u001b[48;5;{number}m A {number} \u001b[0m", end=" ")

    print("\u001b[0m")

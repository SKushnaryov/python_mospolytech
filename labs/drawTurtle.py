import turtle

MINIMUM_LENGTH = 5


def build_tree(t, length, shorten_by, angle):
    if length > MINIMUM_LENGTH:
        t.forward(length)
        new_length = length - shorten_by

        t.left(angle)
        build_tree(t, new_length, shorten_by, angle)
        t.right(angle * 2)
        build_tree(t, new_length, shorten_by, angle)
        t.left(angle)
        t.backward(length)


turtleTree = turtle.Turtle()
turtleTree.speed(0)
turtleTree.hideturtle()
turtleTree.setheading(90)
turtleTree.color('green')
build_tree(turtleTree, 50, 5, 30)
turtle.mainloop()

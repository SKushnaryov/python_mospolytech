# Красивое отображение размера файлов

import os

from enum import Enum


class SizeType(Enum):
    BYTE = "b"
    KB = "Kb"
    MB = "Mb"
    GB = "Gb"


def pretty_size(size_bytes: int, size_type: SizeType) -> str:
    if size_type == SizeType.BYTE:
        return f"{size_bytes} b"
    elif size_type == SizeType.KB:
        return f"{round(size_bytes / 1024, 5)} Kb"
    elif size_type == SizeType.MB:
        return f"{round(size_bytes / 1024 / 1024, 5)} Mb"
    elif size_type == SizeType.GB:
        return f"{round(size_bytes / 1024 / 1024 / 1024, 10)} Gb"


# Добавить столбец отображения размера файла только в Мб
def getSizeInMb(size_bytes: int) -> float:
    return round(size_bytes / 1024 / 1024, 5)


# Добавить столбец отображения размера файла по разрядно (00 000)
def getRankedNumbers(size_bytes: int) -> str:
    str_number = ""
    while size_bytes != 0:
        str_number = f"{size_bytes % 1000}".rjust(3, '0') + " " + str_number
        size_bytes = size_bytes // 1000

    return str_number


os.chdir('C:\\')
print(os.getcwd())

for item in os.listdir():
    if not os.path.isfile(item):
        continue
    file_size = os.path.getsize(item)
    print(f'{item:30} | {file_size:15} | {getSizeInMb(file_size):10} Mb | {getRankedNumbers(file_size):20} | {pretty_size(file_size, SizeType.GB)}')

# Добавить столбец отображения размера файла автоматически (ту в байтах, Кб, Мб, Гб, ...)

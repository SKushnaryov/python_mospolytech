import turtle

t = turtle.Turtle()


def forward_and_turn_turtle(length):
    t.forward(length)
    t.left(90)


def draw_rectangle(rectangleWidth, rectangleHeight, rectangleColor):
    t.color(rectangleColor)
    t.begin_fill()

    forward_and_turn_turtle(rectangleWidth)
    forward_and_turn_turtle(rectangleHeight)
    forward_and_turn_turtle(rectangleWidth)
    forward_and_turn_turtle(rectangleHeight)

    t.end_fill()


if __name__ == '__main__':
    width = int(turtle.textinput("Width", "Enter a width"))
    height = int(turtle.textinput("Height", "Enter a height"))
    color = turtle.textinput("Color", "Enter a color")

    draw_rectangle(
        width,
        height,
        color
    )

turtle.done()

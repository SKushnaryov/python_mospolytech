import random
from utils import printDivider

temp = list(range(10))
print(temp)
print(*temp)

printDivider()

temp = [x * 2 for x in range(10)]
print(temp)
print([x * 2 for x in range(10)])

printDivider()

print([x for x in range(100) if x % 5 == 0])

print([x for x in range(20) if x % 2])
print([x for x in range(20) if x % 2 == 0])

printDivider()

text = "съешь ещё этих мягких французских булок, да выпей чаю"
print([x[0] for x in text.split()])
print([x[-1] for x in text.split()])
print([x[-2] for x in text.split()])

printDivider()
print([len(x) for x in text.split()])
print([x.center(13, '-') for x in text.split()])

printDivider()

# Вернуть список слов
print(text.split())

printDivider()

# Вернуть список слов и рядом с каждым словом его длину  в []
print([f"{x} [{len(x)}]" for x in text.split()])

printDivider()

# Вернуть список слов и рядом с каждым словом его порядковый номер () в предложении
print([f"{x} ({index})" for index, x in enumerate(text.split())])

printDivider()

# В каждом слове поменять буквы местами
print([''.join(reversed(x)) for x in text.split()])

printDivider()

# В каждом слове поменять буквы местами случайным образом
print([''.join(random.sample(x, len(x))) for x in text.split()])

printDivider()

# Каждую последнюю букву в слове с большой буквы
text_list = text.split()
for i in range(len(text_list)):
    chars = list(text_list[i])
    chars[len(text_list[i]) - 1] = chars[len(text_list[i]) - 1].capitalize()
    text_list[i] = ''.join(chars)
print(' '.join(text_list, ))

printDivider()

# В каждом слове первую и последнюю букву заменить на '_'
text_list = text.split()
for i in range(len(text_list)):
    chars = list(text_list[i])
    chars[0] = '_'
    chars[len(text_list[i]) - 1] = '_'
    text_list[i] = ''.join(chars)
print(' '.join(text_list, ))

printDivider()

# Каждое второе слово написать большими буквами
print([x.capitalize() if index % 2 == 1 else x for index, x in enumerate(text.split())])

printDivider()

# Заменить все гласные на '_'
text_list = text.split()
vowels = ["А", "Е", "Ё", "И", "О", "У", "Ы", "Э", "Ю", "Я",
          "а", "е", "ё", "и", "о", "у", "ы", "э", "ю", "я"]

for vowel in vowels:
    for i in range(len(text_list)):
        text_list[i] = text_list[i].replace(vowel, '_')
print(' '.join(text_list))

printDivider()

# Поменять слова местами случайным образом
text_list = text.split()
random.shuffle(text_list)
print(*text_list)

printDivider()

# labTOPFilesInDir

import os
from collections import Counter
from utils import printDivider

DIR_PATH = 'C:\\Program Files'
os.chdir(DIR_PATH)

file_name_list = list()
ext_counter = Counter()
size_counter = Counter()


def fullDirectoryScan(dir_path):
    try:
        with os.scandir(dir_path) as entries:
            for entry in entries:
                if entry.is_file():
                    file_name_list.append(entry.name)

                    countFileExtension(entry.name)

                    save_file_size(entry)
                elif entry.is_dir():
                    fullDirectoryScan(entry.path)
    except PermissionError:
        return


def save_file_size(entry):
    size = os.path.getsize(entry.path)
    if size != 0:
        size_counter[entry.name] = size


def countFileExtension(file_name: str):
    split_list = file_name.split('.')
    if len(split_list) < 2:
        return
    else:
        ext_counter[split_list[-1]] += 1


print("Сканируем файлы")

fullDirectoryScan(DIR_PATH)

print("Топ самых часто используемых расширений файло")
for extCountTuple in ext_counter.most_common()[:10]:
    print(f'{extCountTuple[0]:10} - {extCountTuple[1]}')
printDivider()

print("Топ самых длинных расширений файлов")
for extension in sorted(ext_counter.keys(), key=len, reverse=True)[:10]:
    print(f"{extension:25} - {len(extension)}")
printDivider()

print("Топ самых коротких расширений файлов")
for extension in sorted(ext_counter.keys(), key=len)[:10]:
    print(f"{extension:25} - {len(extension)}")
printDivider()

print("Топ самых длинных имен файлов")
for file_name in sorted(file_name_list, key=len, reverse=True)[:10]:
    print(f"{file_name:100} - {len(file_name)}")
printDivider()

print("Топ самых коротких имён файлов")
for file_name in sorted(file_name_list, key=len)[:10]:
    print(f"{file_name:10} - {len(file_name)}")
printDivider()

print("Топ самых больших файлов")
for file_size_tuple in size_counter.most_common()[:10]:
    print(f"{file_size_tuple[0]} - {file_size_tuple[1] // 1024 // 1024} Mb")
printDivider()

print("Топ самых маленьких файлов")
for file_size_tuple in reversed(size_counter.most_common()[-10:]):
    print(f"{file_size_tuple[0]} - {file_size_tuple[1]}")
'''
Добавить константу путь к папке
Топ 5
Топ самых часто используемых расширений файлов
Топ самых больших файлов
Топ самых маленьких файлов
Топ самых длинных имен файлов
Топ самых длинных расширений файлов

Топ самых коротких имен файлов
Топ самых коротких расширений файлов
'''

# labTOPCharsInFile

from collections import Counter
from utils import printDivider
import re

# Показать статистику всех символов
print("Показать статистику всех символов")
with open('chars_lab_file.txt') as f:
    c = Counter(f.read())
    print(c)
    for key, value in c.items():
        print(f'{repr(key):5} - {value}')
printDivider()

# Показать статистику всех символов в порядке убывания
print("Показать статистику всех символов в порядке убывания")
with open('chars_lab_file.txt') as f:
    c = Counter(f.read())
    for charCountTuple in c.most_common():
        print(f'{repr(charCountTuple[0]):5} - {charCountTuple[1]}')
printDivider()

# Показать статистику всех цифр в порядке убывания
print("Показать статистику всех цифр в порядке убывания")
with open('chars_lab_file.txt') as f:
    numbers = re.findall(r'[0-9]', f.read())
    for numberCountTuple in Counter(numbers).most_common():
        print(f'{repr(numberCountTuple[0]):5} - {numberCountTuple[1]}')
printDivider()

# Показать статистику всех английских букв в порядке уменьшения (все буквы должны быть и напротив кол-во встречаний)
print("Показать статистику всех английских букв в порядке уменьшения")
with open('chars_lab_file.txt') as f:
    numbers = re.findall(r'[A-Za-z]', f.read())
    for numberCountTuple in Counter(numbers).most_common():
        print(f'{repr(numberCountTuple[0]):5} - {numberCountTuple[1]}')

'''
Создать имя файла
Показать статистику всех символов
Показать статистику всех символов в порядке убывания
Показать статистику всех цифр в порядке убывания
Показать статистику всех английских букв в порядке уменьшения (все буквы должны быть и напротив кол-во встречаний)
'''

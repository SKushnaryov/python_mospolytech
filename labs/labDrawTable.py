def fillTableBorders(table, width, height):
    table[0][0] = "┌"
    table[height - 1][0] = "└"
    table[0][width - 1] = "┐"
    table[height - 1][width - 1] = "┘"

    for i in range(1, width - 1):
        if i % 2 == 0:
            table[0][i] = "┬"
            table[height - 1][i] = "┴"
        else:
            table[0][i] = "─"
            table[height - 1][i] = "─"

    for i in range(1, height - 1):
        if i % 2 == 0:
            table[i][0] = "├"
            table[i][width - 1] = "┤"
        else:
            table[i][0] = "│"
            table[i][width - 1] = "│"


def fillRowColumnLines(table, width, height):
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            if j % 2 == 0 and i % 2 != 0:
                table[i][j] = "│"
            elif j % 2 != 0 and i % 2 == 0:
                table[i][j] = "─"
            elif j % 2 == 0 and i % 2 == 0:
                table[i][j] = "┼"


def drawTable(rows, columns):
    if rows == 0 or columns == 0:
        return
    width = 3 + rows * 2 - 2
    height = 3 + columns * 2 - 2
    table = [[" " for _ in range(width)] for _ in range(height)]

    fillTableBorders(table, width, height)
    fillRowColumnLines(table, width, height)

    for row in table:
        for val in row:
            print(val, end="")
        print()


if __name__ == '__main__':
    rows = int(input("Введите количество строк: "))
    columns = int(input("Введите количество колонок: "))
    drawTable(rows, columns)
    input()

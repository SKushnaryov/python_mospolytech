from enum import Enum
from random import randint


class GameMode(Enum):
    DEC_MODE = "Ответ десятичным числом",
    HEX_MODE = "Ответ шестнадцатеричным числом "


class DifficultyLevel(Enum):
    CHILD = "Детский"
    EASY = "Лёгкий"
    MEDIUM = "Средний"
    HARD = "Сложный"


def checkDecAnswer(question: int, answer: int) -> bool:
    return question == answer


def checkHexAnswer(question: int, answer: str) -> bool:
    return f"{question:x}" == answer


def checkAnswer(question: int, answer: str, game_mode: GameMode) -> bool:
    if game_mode == GameMode.DEC_MODE:
        return checkDecAnswer(question, int(answer))
    elif game_mode == GameMode.HEX_MODE:
        return checkHexAnswer(question, answer)


def chooseGameMode() -> GameMode:
    mode = input("Выберите режим игры:\n[1] Ответ десятичным числом\n[2] Ответ шестнадцатеричным числом\n")
    if not mode.isnumeric():
        print("Не могу определить режим игры. Повторите попытку!")
        return chooseGameMode()
    elif int(mode) == 1:
        return GameMode.DEC_MODE
    elif int(mode) == 2:
        return GameMode.HEX_MODE


def chooseDifficultyLevel() -> DifficultyLevel:
    level = input("Выберете сложность игры:\n[1] Детский\n[2] Лёгкий\n[3] Средний\n[4] Сложный\n")
    if not level.isnumeric():
        print("Не могу определить сложность игры. Повторите попытку")
        return chooseDifficultyLevel()
    elif int(level) == 1:
        return DifficultyLevel.CHILD
    elif int(level) == 2:
        return DifficultyLevel.EASY
    elif int(level) == 3:
        return DifficultyLevel.MEDIUM
    elif int(level) == 4:
        return DifficultyLevel.HARD


def chooseNumberOfQuestion() -> int:
    number_of_questions = input("Выберете количество вопросов: ")

    if not number_of_questions.isnumeric():
        print("Не могу определить количество вопросов. Повторите попытку:")
        return chooseNumberOfQuestion()

    return int(number_of_questions)


def setMaxNumber(difficulty_level: DifficultyLevel) -> int:
    if difficulty_level == DifficultyLevel.CHILD:
        return 7
    elif difficulty_level == DifficultyLevel.EASY:
        return 15
    elif difficulty_level == DifficultyLevel.MEDIUM:
        return 63
    elif difficulty_level == DifficultyLevel.HARD:
        return 255


def game():
    print("Начало игры: ")

    game_mode = chooseGameMode()
    difficulty_level = chooseDifficultyLevel()
    number_of_questions = chooseNumberOfQuestion()

    max_number = setMaxNumber(difficulty_level)

    correct_answers = 0
    incorrect_answers = 0

    for i in range(number_of_questions):
        number = randint(0, max_number)

        answer = input(f"Вопрос #{i} {number:b} = ? dec\nОтвет: ") if game_mode == GameMode.DEC_MODE else input(
            f"{number:b} = ? hex\nОтвет: ")

        if checkAnswer(number, answer, game_mode):
            correct_answers = correct_answers + 1
        else:
            incorrect_answers = incorrect_answers + 1

    print(
        "----------------------------------\n"
        "            КОНЕЦ ИГРЫ            \n"
        "----------------------------------\n"
        f"Выбранный режим: {game_mode.value}\n"
        f"Выбранная сложность: {difficulty_level.value}\n"
        f"Общее количество вопросов: {number_of_questions}\n"
        f"Результаты:\n"
        f"Количество верных ответов: {correct_answers}\n"
        f"Количество неправильных ответов: {incorrect_answers}\n"
        "----------------------------------"
    )
    retry = input("Повторить игру? Да/Нет ")
    if retry == "Да":
        print("----------------------------\n")
        game()


if __name__ == '__main__':
    game()

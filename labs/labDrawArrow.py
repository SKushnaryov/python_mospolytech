from enum import Enum


class Direction(Enum):
    LEFT = "влево"
    RIGHT = "вправо"
    TOP = "вверх"
    DOWN = "вниз"


if __name__ == '__main__':
    direction = input('Введите направление: ').lower()
    if direction == Direction.LEFT.value:
        print(u'\u2192')
    elif direction == Direction.RIGHT.value:
        print(u'\u2190')
    elif direction == Direction.TOP.value:
        print(u'\u2191')
    elif direction == Direction.DOWN.value:
        print(u'\u2193')
    else:
        print('Не получилось определить направление')

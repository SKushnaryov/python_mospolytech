def drawSnail(width, height):
    snail_matrix = [[" " for _ in range(width)] for _ in range(height)]

    width_right_index = width
    height_bottom_index = height

    width_left_index = -2
    height_top_index = 0

    while True:
        if width_left_index - width_right_index >= 2 or width_left_index == width_right_index or height_top_index - height_bottom_index >= 2:
            break

        for i in range(width_left_index, width_right_index):
            if i < 0:
                continue
            if height_top_index - height_bottom_index == 1:
                snail_matrix[height_bottom_index][i] = "#"
            else:
                snail_matrix[height_top_index][i] = "#"
        width_left_index = width_left_index + 2

        for i in range(height_top_index, height_bottom_index):
            snail_matrix[i][width_right_index - 1] = "#"
        height_top_index = height_top_index + 2

        for i in range(width_left_index, width_right_index):
            if i < 0 or height_top_index - height_bottom_index >= 2:
                continue
            if height_top_index - height_bottom_index == 1:
                snail_matrix[height_top_index - 1][i] = "#"
            else:
                snail_matrix[height_bottom_index - 1][i] = "#"
        width_right_index = width_right_index - 2

        for i in range(height_top_index, height_bottom_index):
            snail_matrix[i][width_left_index] = "#"
        height_bottom_index = height_bottom_index - 2

    for line in snail_matrix:
        print(''.join(line))


if __name__ == '__main__':
    drawSnail(22, 22)
    print("---------------")
    drawSnail(19, 25)
    print("---------------")
    drawSnail(18, 30)
    drawSnail(30, 30)
    print("---------------------")
    drawSnail(30, 10)
    print("---------------")
    drawSnail(
        int(input("Введите ширину улитки: ")),
        int(input("Введите высоту улитки: "))
    )
